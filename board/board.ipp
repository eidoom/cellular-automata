#include <algorithm>
#include <iostream>

template <typename T>
board::Board<T>::Board(const int ii_, const int jj_)
    : std::vector<std::vector<T>>(ii_, std::vector<T>(jj_))
    , ii(ii_)
    , jj(jj_)
{
}

template <typename T>
board::Board<T>::Board(const int l_)
    : std::vector<std::vector<T>>(l_, std::vector<T>(l_))
    , ii(l_)
    , jj(l_)
{
}

template <typename T>
void board::Board<T>::append_i()
{
    this->resize(ii, std::vector<T>(jj));
}

template <typename T>
void board::Board<T>::append_j()
{
    for (std::vector<T>& v : *this) {
        v.resize(jj);
    }
}

template <typename T>
void board::Board<T>::prepend_i(const int di_)
{
    this->mi += di_;
    this->ii += di_;
    const std::vector<std::vector<T>> newVec(this->di, std::vector<T>(jj));
    this->insert(this->begin(), newVec.begin(), newVec.end());
}

template <typename T>
void board::Board<T>::prepend_j(const int dj_)
{
    this->mj += dj_;
    this->jj += dj_;
    for (std::vector<T>& v : *this) {
        const std::vector<T> newVec(dj_);
        v.insert(v.begin(), newVec.begin(), newVec.end());
    }
}

template <typename T>
int board::Board<T>::move_i()
{
    const int mi_ { this->mi };
    this->mi = 0;
    return mi_;
}

template <typename T>
int board::Board<T>::move_j()
{
    const int mj_ { this->mj };
    this->mj = 0;
    return mj_;
}

template <typename T>
bool board::Board<T>::moved() const
{
    return ((this->mi != 0) || (this->mj != 0));
}

template <typename T>
void board::Board<T>::safe_bounds(const int i, const int j)
{
    // void board::Board<T>::safe_bounds(std::mutex& mtx, const int i, const int j)
    // {
    //     std::scoped_lock<std::mutex> locked(mtx);
    if (i < 0) {
        this->prepend_i(-i + 1);
    }
    if (j < 0) {
        this->prepend_j(-j + 1);
    }
    if (i >= this->ii) {
        this->ii = i + 1;
        this->append_i();
    }
    if (j >= this->jj) {
        this->jj = j + 1;
        this->append_j();
    }
}

template <typename T>
void board::Board<T>::check_lower_bounds(const int i, const int j)
{
    if (i - 1 < 0) {
        di = true;
    }
    if (j - 1 < 0) {
        dj = true;
    }
}

template <typename T>
void board::Board<T>::check_upper_bounds(const int i, const int j)
{
    if (i + 1 >= this->ii) {
        ui = true;
    }
    if (j + 1 >= this->jj) {
        uj = true;
    }
}

template <typename T>
void board::Board<T>::extend_bounds(std::mutex& mtx)
{
    std::scoped_lock<std::mutex> locked(mtx);
    if (di) {
        this->prepend_i(1);
        di = false;
    }
    if (dj) {
        this->prepend_j(1);
        dj = false;
    }
    if (ui) {
        this->ii += 1;
        this->append_i();
        ui = false;
    }
    if (uj) {
        this->jj += 1;
        this->append_j();
        uj = false;
    }
}

// template <typename T>
// typename board::Board<T>::ref board::Board<T>::operator()(std::mutex& mtx, const int i, const int j)
// {
//     this->safe_bounds(mtx, i, j);
//     return (*this)[i][j];
// }

template <typename T>
typename board::Board<T>::ref board::Board<T>::operator()(const int i, const int j)
{
    this->safe_bounds(i, j);
    return (*this)[i][j];
}

template <typename T>
T board::Board<T>::get(std::mutex& mtx, const int i, const int j) const
{
    std::scoped_lock<std::mutex> locked(mtx);
    return this->get(i, j);
}

template <typename T>
T board::Board<T>::get(const int i, const int j) const
{
    if ((i < 0) || (i >= this->ii) || (j < 0) || (j >= this->jj)) {
        return T();
    } else {
        return (*this)[i][j];
    }
}

template <typename T>
int board::Board<T>::width() const
{
    return this->jj;
}

template <typename T>
int board::Board<T>::height() const
{
    return this->ii;
}
