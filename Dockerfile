FROM alpine:3.12

ENV DISPLAY :1
ENV UID 1000
ENV GID 1000

RUN apk --no-cache --no-progress add curl g++ make sfml-dev

# RUN mkdir -p /home/developer
# RUN echo "developer:x:${UID}:${GID}:Developer,,,:/home/developer:/bin/bash" >> /etc/passwd
# RUN echo "developer:x:${UID}:" >> /etc/group
# RUN echo "developer ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
# RUN chmod 0440 /etc/sudoers
# RUN chown ${UID}:${GID} -R /home/developer
# USER developer
# ENV HOME /home/developer

RUN adduser -D --uid ${UID} -g ${GID} user
USER user
ENV HOME /home/user

WORKDIR $HOME

RUN curl --output cellular-automata-1.2.1.tar.gz https://gitlab.com/eidoom/cellular-automata/uploads/10d99be37787579e186018a57e41fd30/cellular-automata-1.2.1.tar.gz

RUN tar -xzf cellular-automata-1.2.1.tar.gz

# RUN rm cellular-automata-1.2.1.tar.gz

WORKDIR $HOME/cellular-automata-1.2.1

RUN ./configure

RUN make

WORKDIR $HOME/cellular-automata-1.2.1/src

CMD ["./main", "100", "100", "10"]


