#include <algorithm>

#include "config.hpp"

void config::parameters::set_update_period()
{
    this->update_period = std::chrono::microseconds(1000000 / this->update_rate);
}

void config::parameters::reset_update_period()
{
    this->update_rate = this->init_update_rate;
    this->set_update_period();
}

void config::parameters::decrease_update_period()
{
    this->update_rate += this->delta_updates;
    this->set_update_period();
}

void config::parameters::increase_update_period()
{
    this->update_rate = std::max(this->update_rate - this->delta_updates, 1);
    this->set_update_period();
}

void config::parameters::reset_velocity()
{
    this->velocity = this->init_velocity;
}

config::parameters::parameters(const int num_i_, const int num_j_, const int upd_rate_)
    : init_update_rate(upd_rate_)
    , num_i(num_i_)
    , num_j(num_j_)
    , height(static_cast<int>(num_i_ * this->size))
    , width(static_cast<int>(num_j_ * this->size))
    , update_rate(upd_rate_)
    , centre_x(0.f)
    , centre_y(0.f)
{
    this->set_update_period();
}
