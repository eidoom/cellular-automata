#pragma once

#include <mutex>

#include "board.hpp"

namespace life {

class Universe : public board::Board<bool> {
private:
    Board<bool> prevState;

    void logic(int i, int j);

public:
    std::mutex mtx;

    bool get(int i, int j);

    void tick();

    // start with 1 in <bias> cells alive in a random distribution
    // eg. bias = 3 means one third of cells start alive
    // default is equal probability for dead or alive
    // bias = 0 means all cells start alive
    void random(int bias = 2);

    void gosper(int i = 0, int j = 0);

    void beacon(int a = 0, int b = 0);

    void glider(int i = 0, int j = 0);

    void block();

    // h : initial height in cells (i-axis)
    // w : initial widht in cells (j-axis)
    Universe(int h = 100, int w = 100);
};

} // namespace life
