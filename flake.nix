{
  description = "Autotools/C++ dev env";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-24.11";
  };

  outputs = { self, nixpkgs }: let
    system = "x86_64-linux";
    pkgs = import nixpkgs { inherit system; };
  in {
    devShells."${system}".default = pkgs.mkShell {
      packages = with pkgs; [
        autoconf
        automake
        gcc
        gnumake
        libtool
        sfml
      ];
    };
  };
}
