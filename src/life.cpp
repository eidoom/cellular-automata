#include <iostream>
#include <random>

#include "life.hpp"

life::Universe::Universe(const int h, const int w)
    : board::Board<bool>(h, w)
    , prevState(h, w)
{
}

// logic for Conway's Game of Life, B3/S23
void life::Universe::logic(const int i, const int j)
{
    int neighbourhood { 0 };
    for (int a { i - 1 }; a < i + 2; ++a) {
        for (int b { j - 1 }; b < j + 2; ++b) {
            if (prevState.get(a, b)) {
                ++neighbourhood;
            }
        }
    }
    if (neighbourhood == 3) {
        // B3/S2
        (*this)[i][j] = 1;
        this->check_upper_bounds(i + 1, j + 1);
        this->check_lower_bounds(i - 1, j - 1);
    } else if (neighbourhood != 4) {
        // S3
        (*this)[i][j] = 0;
    }
}

void life::Universe::tick()
{
    prevState = *this;
    for (int i { 0 }; i < prevState.height(); ++i) {
        for (int j { 0 }; j < prevState.width(); ++j) {
            this->logic(i, j);
        }
    }
    this->extend_bounds(this->mtx);
}

bool life::Universe::get(const int i, const int j)
{
    return board::Board<bool>::get(this->mtx, i, j);
}

void life::Universe::beacon(const int a, const int b)
{
    for (int i { 1 }; i < 3; ++i) {
        for (int j { 1 }; j < 3; ++j) {
            (*this)(a + i, b + j) = 1;
        }
    }
    for (int i { 3 }; i < 5; ++i) {
        for (int j { 3 }; j < 5; ++j) {
            (*this)(a + i, b + j) = 1;
        }
    }
}

void life::Universe::glider(const int i, const int j)
{
    for (int k { 1 }; k < 4; ++k) {
        (*this)(i + k, j + 3) = 1;
    }
    (*this)(i + 3, j + 2) = 1;
    (*this)(i + 2, j + 1) = 1;
}

void life::Universe::random(const int bias)
{
    std::random_device dev;
    std::mt19937_64 rng(dev());
    std::uniform_int_distribution<int> dist01(0, bias);

    // leave dead cells around edges or life logic will fail
    for (int i { 1 }; i < this->height() - 1; ++i) {
        for (int j { 1 }; j < this->width() - 1; ++j) {
            (*this)(i, j) = (dist01(rng) == bias);
        }
    }
}

void life::Universe::gosper(const int i, const int j)
{
    for (int a { 0 }; a < 2; ++a) {
        for (int b { 0 }; b < 2; ++b) {
            (*this)(i + 4 + a, j + b) = 1;
        }
    }

    for (int a { 0 }; a < 2; ++a) {
        for (int b { 0 }; b < 2; ++b) {
            (*this)(i + 2 + a, j + 34 + b) = 1;
        }
    }

    for (int a { 0 }; a < 3; ++a) {
        for (int b { 0 }; b < 2; ++b) {
            (*this)(i + 2 + a, j + 20 + b) = 1;
        }
    }

    (*this)(i + 1, j + 22) = 1;
    (*this)(i + 0, j + 24) = 1;
    (*this)(i + 1, j + 24) = 1;

    (*this)(i + 5, j + 22) = 1;
    (*this)(i + 6, j + 24) = 1;
    (*this)(i + 5, j + 24) = 1;

    for (int a { 0 }; a < 3; ++a) {
        (*this)(i + 4 + a, j + 10) = 1;
    }

    (*this)(i + 3, j + 11) = 1;
    (*this)(i + 2, j + 12) = 1;
    (*this)(i + 2, j + 13) = 1;
    (*this)(i + 3, j + 15) = 1;

    (*this)(i + 7, j + 11) = 1;
    (*this)(i + 8, j + 12) = 1;
    (*this)(i + 8, j + 13) = 1;
    (*this)(i + 7, j + 15) = 1;

    for (int a { 0 }; a < 3; ++a) {
        (*this)(i + 4 + a, j + 16) = 1;
    }

    (*this)(i + 5, j + 14) = 1;
    (*this)(i + 5, j + 17) = 1;
}

void life::Universe::block()
{
    (*this)(1, 1) = 1;
    (*this)(1, 2) = 1;
    (*this)(2, 1) = 1;
    (*this)(2, 2) = 1;
}
