#pragma once

#include <vector>
#include <type_traits>
#include <mutex>

namespace board {

template <typename T>
class Board : private std::vector<std::vector<T>> {
private:
    using ref = typename std::conditional<std::is_same<T, bool>::value, std::vector<bool>::reference, T&>::type;

    int view_width;
    int view_height;
    int ii;
    int jj;

    void append_i();
    void append_j();

    void prepend_i(int di_);
    void prepend_j(int dj_);

    void safe_bounds(int i, int j);
    // void safe_bounds(std::mutex& mtx, int i, int j);

    bool di { false };
    bool dj { false };

    bool ui { false };
    bool uj { false };

    int mi { 0 };
    int mj { 0 };

protected:
    void check_lower_bounds(int i, int j);
    void check_upper_bounds(int i, int j);

    void extend_bounds(std::mutex& mtx);

    // provides (index) safe access; use index operators [][] to skip bound checks
    ref operator()(int i, int j);
    // currently only used in initialisation, so didn't make thread safe
    // ref operator()(std::mutex& mtx, int i, int j);

    // provides unsafe access; use index operators ()() to use bound checks
    using std::vector<std::vector<T>>::operator[];

public:
    // safe (indexing and thread) const access
    T get(std::mutex& mtx, int i, int j) const;

    // safe (indexing but not thread) const access
    T get(int i, int j) const;

    int width() const;
    int height() const;

    bool moved() const;
    int move_i();
    int move_j();

    // for rectangular board of width jj_ and height ii_ in cells
    Board(int ii_ = 100, int jj_ = 100);

    // for square board of size-length l_ in cells
    Board(int l_ = 100);
};

} // namespace board

#include "board.ipp"
