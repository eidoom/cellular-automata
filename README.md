# [cellular-automata](https://gitlab.com/eidoom/cellular-automata)

* [Companion post](https://computing-blog.netlify.app/post/cellular-automata/)

## Background

### Theory

This runs the [cellular automaton](https://en.wikipedia.org/wiki/Cellular_automaton) [Conway's Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life).

### Technology

I created this project to become more familiar with 
* the programming language [C++](https://en.wikipedia.org/wiki/C%2B%2B)
* the automated build system [GNU Autotools](https://en.wikipedia.org/wiki/GNU_Autotools), including
    * [Autoconf](https://en.wikipedia.org/wiki/Autoconf)
    * [Automake](https://en.wikipedia.org/wiki/Automake)
    * [Libtool](https://en.wikipedia.org/wiki/GNU_Libtool)

## Usage

These instructions are for Fedora.
Tested on Fedora 31-33.
They should also work on other Linux distributions, although package names may differ for alternative package managers, and this is untested.

### Dependencies

* Requires a C++ compiler. Install either of
    * GCC
        ```shell
        sudo dnf install gcc
        ```
    * Clang
        ```shell
        sudo dnf install clang
        ```
* Requiries `make`
    ```shell
    sudo dnf install make
    ```
* Uses [SFML](https://www.sfml-dev.org/)
    ```shell
    sudo dnf install SFML-devel
    ```

### Build

#### Build from release (recommended)

* Download the latest release from [releases](https://gitlab.com/eidoom/cellular-automata/-/releases), unarchive and compile.
* For example, for version 1.0, click the asset `cellular-automata-1.0.tar.gz` to download it, or use
```shell
wget https://gitlab.com/eidoom/cellular-automata/uploads/cb9455f9199c9c370042e49c0ee8266d/cellular-automata-1.0.tar.gz
```
then
```shell
VERSION=1.0
tar -xzvf cellular-automata-VERSION.tar.gz
cd cellular-automata-VERSION
mkdir build
cd build
../configure
make -j
```

#### Build from VCS (alternative)

* Additional dependencies (Autotools)
```shell
sudo dnf install autoconf automake libtool
```
* Get and build with
```shell
git clone git@gitlab.com:eidoom/cellular-automata.git
cd cellular-automata
autoreconf -i
mkdir build
cd build
../configure
make -j
```

### Run

Run with
```shell
cd src
./main <height> <width> <framerate>
```
where:
* `height` is in pixels
* `width` is in pixels
* `framerate` is in frames per second

For example,
```shell
./main 1000 1000 10
```

When the program is running, control the view with:
* `wasd` for movement
* `zx` for zooming
* `c` to reset position and zoom
* `rf` for altering simulation speed
* `v` to reset simulation speed

## Development

### Reconfigure build

Automatically reconfigure the build system with
```shell
autoreconf -fi
```
after changes, for example, editing `src/Makefile.am`.

### Releases

To package new releases, first bump up the version in `configure.ac`, then
```shell
cd build
../configure
make dist
```
then [add them to GitLab](https://gitlab.com/eidoom/cellular-automata/-/tags/new).
Tag name should be of the form `v1.1`, include release notes, and use the `Attach a file` button to upload the packaged release.
The asset URL will be pasted into the release notes as Markdown; copy it out and prepended it with the [project URL](https://gitlab.com/eidoom/cellular-automata/), to add it to the new [release](https://gitlab.com/eidoom/cellular-automata/-/releases).

### TODO

* View judders when board resizes
    * This is probably because the rendering thread runs faster than the logic thread, so renders frames from inside the prepend step of increasing the world size

### Container

Note: `docker` doesn't work on Fedora 31+, so using `podman` instead.

Image builds with `podman build --rm -t eidoom/cellular-automata:v1.2.1 .`.

Run with `podman run -it --rm -e DISPLAY=$DISPLAY -e UID=1000 -e GID=1000 -v /tmp/.X11-unix:/tmp/.X11-unix eidoom/cellular-automata:v1.2.1
`.

App fails to open X11 display.

Not yet pushed to a remote repo since WIP.
