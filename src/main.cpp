#include <algorithm>
#include <chrono>
#include <cstdlib>
#include <iostream>
#include <mutex>
#include <thread>

#include <SFML/Graphics.hpp>

#include "config.hpp"
#include "life.hpp"

void logic(life::Universe* uni, std::chrono::microseconds* update_period_)
{
    while (true) {
        std::chrono::time_point<std::chrono::high_resolution_clock> start { std::chrono::high_resolution_clock::now() };
        uni->tick();
        std::this_thread::sleep_until(start + *update_period_);
    }
}

int main(int argc, char* argv[])
{
    std::cout.sync_with_stdio(false);

    if (argc != 4) {
        std::cerr << "\nUse as ./main <height> <width> <framerate>\n";
        std::exit(EXIT_FAILURE);
    }

    const int num_i { std::atoi(argv[1]) };
    const int num_j { std::atoi(argv[2]) };
    const int upd_rate { std::atoi(argv[3]) };

    config::parameters p(num_i, num_j, upd_rate);

    std::cout
        << '\n'
        << "Initial universe height set to " << p.num_i << " cells\n"
        << "Initial universe width set to " << p.num_j << " cells\n"
        << "Update rate set to " << p.update_rate << " updates per second\n"
        << "Frame rate set to " << p.frame_rate << " frames per second\n"
        << std::endl;

    sf::RenderWindow window(sf::VideoMode(p.width, p.height), "Cellular automaton");
    window.setFramerateLimit(p.frame_rate);

    // initialise universe
    life::Universe alpha(p.num_i, p.num_j);

    // define initial conditions
    // alpha.beacon();
    // alpha.block();
    // alpha.glider(0, 0);
    // alpha.gosper(1, 1);
    alpha.random(1);

    std::thread logic_thread(logic, &alpha, &p.update_period);

    while (window.isOpen()) {

        if (alpha.moved()) {
            sf::View newView { window.getView() };
            const float diff_x { p.size * alpha.move_j() };
            const float diff_y { p.size * alpha.move_i() };
            p.centre_x += diff_x;
            p.centre_y += diff_y;
            newView.move(diff_x, diff_y);
            window.setView(newView);
        }

        sf::Event event;
        while (window.pollEvent(event)) {
            switch (event.type) {
            case sf::Event::Closed:
                window.close();
                break;
            // case sf::Event::Resized: {
            //     // doesn't support camera changes from user scrolling and zooming
            //     sf::View newView(sf::Rect<float>(0.f, 0.f, event.size.width, event.size.height));
            //     window.setView(newView);
            //     break;
            // }
            default:
                break;
            }
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Z)) {
            sf::View newView { window.getView() };
            newView.zoom(1.f / p.zoom_rate);
            window.setView(newView);
            p.velocity /= p.zoom_rate;
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::X)) {
            sf::View newView { window.getView() };
            newView.zoom(p.zoom_rate);
            window.setView(newView);
            p.velocity *= p.zoom_rate;
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)) {
            sf::View newView { window.getView() };
            newView.move(0.f, p.velocity);
            window.setView(newView);
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)) {
            sf::View newView { window.getView() };
            newView.move(0.f, -p.velocity);
            window.setView(newView);
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) {
            sf::View newView { window.getView() };
            newView.move(-p.velocity, 0.f);
            window.setView(newView);
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
            sf::View newView { window.getView() };
            newView.move(p.velocity, 0.f);
            window.setView(newView);
        }

        // if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q)) {
        //     sf::View newView { window.getView() };
        //     newView.rotate(15.f);
        //     window.setView(newView);
        // }

        // if (sf::Keyboard::isKeyPressed(sf::Keyboard::E)) {
        //     sf::View newView { window.getView() };
        //     newView.rotate(-15.f);
        //     window.setView(newView);
        // }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::C)) {
            sf::View newView(sf::Rect<float>(p.centre_x, p.centre_y, p.width, p.height));
            window.setView(newView);
            p.reset_velocity();
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::R)) {
            p.decrease_update_period();
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::F)) {
            p.increase_update_period();
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::V)) {
            p.reset_update_period();
        }

        window.clear(sf::Color::Black);
        for (int i { 0 }; i < alpha.height(); ++i) {
            for (int j { 0 }; j < alpha.width(); ++j) {
                if (alpha.get(i, j)) {
                    sf::RectangleShape square(sf::Vector2<float>(p.size, p.size));
                    square.setPosition(j * p.size, i * p.size);
                    square.setFillColor(sf::Color::White);
                    window.draw(square);
                }
            }
        }

#ifdef DEBUG
        // debug: show board border
        for (int i { 0 }; i < alpha.height() + 1; ++i) {
            for (int j { -1 }; j < alpha.width() + 2; j += alpha.width() + 2) {
                sf::RectangleShape square(sf::Vector2<float>(p.size, p.size));
                square.setPosition(j * p.size, i * p.size);
                square.setFillColor(sf::Color::Red);
                window.draw(square);
            }
        }
        for (int j { 0 }; j < alpha.width() + 1; ++j) {
            for (int i { -1 }; i < alpha.height() + 2; i += alpha.height() + 2) {
                sf::RectangleShape square(sf::Vector2<float>(p.size, p.size));
                square.setPosition(j * p.size, i * p.size);
                square.setFillColor(sf::Color::Red);
                window.draw(square);
            }
        }
#endif

        window.display();
    }

    return EXIT_SUCCESS;
}
