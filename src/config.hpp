#pragma once

#include <chrono>

namespace config {

class parameters {
private:
    static constexpr float init_velocity { 10.f };
    static constexpr int delta_updates { 1 };
    const int init_update_rate;

public:
    static constexpr int frame_rate { 60 };
    static constexpr float zoom_rate { 1.02f };
    static constexpr float size { 10.f };
    const int num_i;
    const int num_j;
    const int height;
    const int width;
    int update_rate;
    float velocity { init_velocity };
    float centre_x;
    float centre_y;
    std::chrono::microseconds update_period;

    void set_update_period();

    void reset_update_period();

    void decrease_update_period();

    void increase_update_period();

    void reset_velocity();

    parameters(int num_i_, int num_j_, int upd_rate_);
};

} // namespace config
